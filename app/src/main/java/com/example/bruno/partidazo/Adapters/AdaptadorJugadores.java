package com.example.bruno.partidazo.Adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bruno.partidazo.R;
import com.example.bruno.partidazo.entidades.Jugador;

import java.util.ArrayList;

import static android.view.View.GONE;

public class AdaptadorJugadores extends RecyclerView.Adapter<AdaptadorJugadores.JugadoresViewHolder> implements View.OnClickListener{

    ArrayList<Jugador> listaJugadores;
    private View.OnClickListener listener;
    private int color;

    public AdaptadorJugadores(ArrayList<Jugador> listaJugadores, int color){
        this.listaJugadores = listaJugadores;
        this.color = color;
    }

    @Override
    public JugadoresViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_jugadores, null, false);
        view.setOnClickListener(this);
        if (color == 0) {
            view.setBackgroundColor(Color.BLUE); //MARCO A JUGADORES
        }
        if (color == 1){
            view.setBackgroundColor(Color.GREEN); //MARCO A JUGADORES A AGREGAR/ASOCIAR
        }
        if (color == 2){
            view.setBackgroundColor(Color.RED); //MARCO A JUGADORES A SACAR/DESASOCIAR
        }
        return new AdaptadorJugadores.JugadoresViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JugadoresViewHolder holder, int position) {
        holder.textNombre.setText(listaJugadores.get(position).getNombre());
        holder.textApellido.setText(listaJugadores.get(position).getApellido());
        holder.textUsername.setText(listaJugadores.get(position).getUsername());
        holder.textTelefono.setText(listaJugadores.get(position).getTelefono());
        holder.foto.setImageBitmap(listaJugadores.get(position).getImagen());
        //holder.iconoAsociado.setImageBitmap(listaPartidos.get(position).getIconoAsociado());
        //holder.textSuscriptos.setText(listaPartidos.get(position).getCantSuscriptos());
        if (listaJugadores.get(position).getAsociados() == 0){
            holder.iconoAsociado.setVisibility(View.GONE);
        }
        else{
            holder.iconoAsociado.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return listaJugadores.size();
    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onClick(view);
        }
    }

    public class JugadoresViewHolder extends RecyclerView.ViewHolder{

        TextView textNombre, textApellido, textUsername, textEmail, textTelefono;
        ImageView foto, iconoAsociado;

        public JugadoresViewHolder(View itemView) {
            super(itemView);
            textNombre = (TextView) itemView.findViewById(R.id.id_nombre);
            textApellido = (TextView) itemView.findViewById(R.id.id_apellido);
            textUsername = (TextView) itemView.findViewById(R.id.id_username);
            //textEmail = (TextView) itemView.findViewById(R.id.email);
            textTelefono = (TextView) itemView.findViewById(R.id.id_telefono);
            foto = (ImageView) itemView.findViewById(R.id.id_imagen);
            iconoAsociado = (ImageView) itemView.findViewById(R.id.imagen_asociado);
            iconoAsociado.setVisibility(GONE);
            //textSuscriptos = (TextView) itemView.findViewById(R.id.cant_suscriptos);
        }
    }
}
