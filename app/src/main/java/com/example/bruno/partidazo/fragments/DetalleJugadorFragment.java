package com.example.bruno.partidazo.fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.bruno.partidazo.R;
import com.example.bruno.partidazo.entidades.Jugador;
import com.example.bruno.partidazo.entidades.Respuesta;
import com.example.bruno.partidazo.entidades.Save;
import com.example.bruno.partidazo.interfaces.ComunicacionFragments;

import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetalleJugadorFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetalleJugadorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleJugadorFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    NotificationCompat.Builder mBuilder;
    int mNotificationID = 001;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String DELETE_USUARIO = "https://vitucci.000webhostapp.com/deleteUsuario.php";

    private OnFragmentInteractionListener mListener;
    TextView textUsuario, textNombre, textApellido, textEmail, textTelefono, textSuscriptos; //, textID
    ImageView imagenDetalle, botonEliminar;
    Button botonFloat;
    private String KEY_ID = "id";
    private String KEY_ID_USUARIO = "id_usuario";
    StringRequest peticion;
    boolean flag = false;
    boolean flagToast = false;
    private TreeMap<String, String> descrip;
    Activity activity;
    Context context;
    ComunicacionFragments interfaceComunicacionFragments;
    ListaJugadoresFragment listaJugadoresFragment;
    DetalleJugadorFragment detalleJugadorFragment;
    FragmentManager fm;
    public int PICK_IMAGE_REQUEST = 1;

    public DetalleJugadorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleJugadorFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleJugadorFragment newInstance(String param1, String param2) {
        DetalleJugadorFragment fragment = new DetalleJugadorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View vista = inflater.inflate(R.layout.fragment_detalle_jugador, container, false);
        textUsuario = (TextView) vista.findViewById(R.id.text_usuario);
        textNombre = (TextView) vista.findViewById(R.id.text_nombre);
        textApellido = (TextView) vista.findViewById(R.id.text_apellido);
        textEmail = (TextView) vista.findViewById(R.id.text_email);
        textTelefono = (TextView) vista.findViewById(R.id.text_telefono);

        botonEliminar = vista.findViewById(R.id.boton_eliminar_usuario);
        botonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                confirmEliminar();
            }

        });

        //textCategoria = (TextView) vista.findViewById(R.id.detalle_categoria);
        imagenDetalle = (ImageView) vista.findViewById(R.id.imagen_para_foto);
        imagenDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), getResources().getString(R.string.mantener_boton), Toast.LENGTH_LONG).show();            }
        });

        imagenDetalle.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View arg0) {
                confirmDialog();
                return true;
            }
        });


        Bundle bundleObjeto = getArguments();
        Jugador jugador = null;
        if (bundleObjeto != null){
            jugador = (Jugador) bundleObjeto.getSerializable("objeto3");
            imagenDetalle.setImageBitmap(jugador.getImagen());
            textUsuario.setText(jugador.getId());
            textNombre.setText(jugador.getNombre());
            textApellido.setText(jugador.getApellido());
            textEmail.setText(jugador.getEmail());
            textTelefono.setText(jugador.getTelefono());
            //guardo el id de la respuesta  para usar en el detalle
            String id_usuario = jugador.getId();
            String nombre = jugador.getNombre();
            String apellido = jugador.getApellido();
            String email = jugador.getEmail();
            String telefono = jugador.getTelefono();
            String username = jugador.getUsername();
            SharedPreferences prefRespuesta = getContext().getSharedPreferences("usuario", getActivity().MODE_PRIVATE);
            SharedPreferences.Editor editor1 = prefRespuesta.edit();
            editor1.putString("id_usuario", id_usuario);
            editor1.putString("nombre", nombre);
            editor1.putString("apellido", apellido);
            editor1.putString("email", email);
            editor1.putString("telefono", telefono);
            editor1.putString("username", username);
            editor1.commit();
        }
        return vista;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void eliminarUsuario() {

        //getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
        SharedPreferences prefUsuario = getContext().getSharedPreferences("usuario", getActivity().MODE_PRIVATE);
        final String id_usuario = prefUsuario.getString("id_usuario","");
        final ProgressDialog loading = ProgressDialog.show(getActivity(),getResources().getString(R.string.str_eliminando),getResources().getString(R.string.str_espere),false,false); //getActivity()
        StringRequest stringRequest = new StringRequest(Request.Method.POST, DELETE_USUARIO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Descartar el diálogo de progreso
                        loading.dismiss();
                        //flagToast = true;
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.usuario_eliminado), Toast.LENGTH_LONG).show(); //ROMPE
                        //getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //loading.dismiss();
                        //Toast.makeText(getActivity(), getResources().getString(R.string.sin_conexion) , Toast.LENGTH_LONG).show();
                        //getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                    }
                }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Creación de parámetros
                Map<String,String> params = new Hashtable<String, String>();
                //Agregando de parámetros
                params.put(KEY_ID, id_usuario);
                //Parámetros de retorno
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creación de una cola de solicitudes
        RequestQueue requestQ = Volley.newRequestQueue(getContext()); //getActivity()
        //Agregar solicitud a la cola
        requestQ.add(stringRequest);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private  void notificationPush() {
        Bundle bundleObjeto2 = getArguments();
        Jugador jugador = null;
        if (bundleObjeto2 != null) {
            jugador = (Jugador) bundleObjeto2.getSerializable("objeto3"); //TRAIGO LOS DATOS DEL RECLAMO ACTUAL
        }
        //Intent intent = new Intent(Intent.ACTION_GET_CONTENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //Intent intentGalleria = new Intent(Intent.ACTION_VIEW, MediaStore.Images.Media.EXTERNAL_CONTENT_URI); ABRE LA GALERIA
        Intent intentGalleria = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI); //ABRO LA CARPETA DONDE SE HALLA LA IMAGEN GUARDADA
        intentGalleria.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Intent intentGalleria = new Intent();
        //intentGalleria.setType("image/*");
        //intentGalleria.setAction(Intent.ACTION_GET_CONTENT); //android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        //intentGalleria.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.journaldev.com"));
        PendingIntent pendingIntent = PendingIntent.getActivity(getContext(), 0, intentGalleria, 0);
        mBuilder = new NotificationCompat.Builder(getContext()).setSmallIcon(android.R.drawable.ic_menu_gallery)
                .setLargeIcon(jugador.getImagen()) //TRAIGO LA IMAGEN DEL RECLAMO
                .setContentTitle("Partidazo!").setContentText(getResources().getString(R.string.imagen_guardada) + "/PartidazoApp").setContentIntent(pendingIntent);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(getContext(),
                        0,
                        intentGalleria,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(mNotificationID, mBuilder.build());
    }

    private void confirmEliminar(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(getResources().getString(R.string.eliminar_usuario))
                .setPositiveButton(getResources().getString(R.string.str_confirmar),  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        eliminarUsuario();
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.detach(DetalleJugadorFragment.this);
                        transaction.commit();
                        closefragment();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.str_cancelar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }


    private void confirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setMessage(getResources().getString(R.string.guardar_imagen))
                .setPositiveButton(getResources().getString(R.string.str_confirmar),  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //convertir imagen a bitmap
                        imagenDetalle.buildDrawingCache();
                        Bitmap bmap = imagenDetalle.getDrawingCache();

                        //guardar imagen
                        Save savefile = new Save();
                        savefile.SaveImage(getContext(), bmap);
                        notificationPush();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.str_cancelar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    private void closefragment() {
        getActivity().getFragmentManager().popBackStack();
    }
}
