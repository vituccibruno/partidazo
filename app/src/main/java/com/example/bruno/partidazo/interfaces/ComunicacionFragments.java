package com.example.bruno.partidazo.interfaces;

import com.example.bruno.partidazo.entidades.Jugador;
import com.example.bruno.partidazo.entidades.Partido;
import com.example.bruno.partidazo.entidades.Respuesta;

/**
 * Created by Bruno on 08/04/2018.
 */

public interface ComunicacionFragments {

    public void enviarPartido(Partido partido);
    public void enviarJugador(Jugador jugador);
    public void enviarRespuesta(Respuesta respuesta);
    public void reejecutarGetHttpResponseDatosUser ();

}
