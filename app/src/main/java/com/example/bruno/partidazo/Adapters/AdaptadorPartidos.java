package com.example.bruno.partidazo.Adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bruno.partidazo.R;
import com.example.bruno.partidazo.entidades.Partido;

import java.util.ArrayList;

import static android.app.ProgressDialog.show;
import static android.view.View.GONE;

/**
 * Created by Bruno on 08/04/2018.
 */

public class AdaptadorPartidos extends RecyclerView.Adapter<AdaptadorPartidos.PartidosViewHolder> implements View.OnClickListener{

    ArrayList<Partido> listaPartidos;
    private View.OnClickListener listener;
    private int color;

    public AdaptadorPartidos(ArrayList<Partido> listaPartidos, int color){
        this.listaPartidos = listaPartidos;
        this.color = color;
    }

    @Override
    public PartidosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        view.setOnClickListener(this);
        if (color == 0) {
            view.setBackgroundColor(Color.BLUE); //MARCO A RECLAMOS
        }
        if (color == 1){
            view.setBackgroundColor(Color.GREEN); //MARCO A RECLAMOS A ASOCIAR
        }
        if (color == 2){
            view.setBackgroundColor(Color.RED); //MARCO A RECLAMOS A DESASOCIAR
        }
        return new PartidosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PartidosViewHolder holder, int position) {
        holder.textFecha.setText(listaPartidos.get(position).getFecha());
        holder.textHora.setText(listaPartidos.get(position).getHora());
        holder.textCancha.setText(listaPartidos.get(position).getCancha());
        //holder.textComentario.setText(listaPartidos.get(position).getComentarioDesc());

        //holder.textCategoria.setText(listaPartidos.get(position).getId_categoria());
        //holder.textEstado.setText(listaPartidos.get(position).getId_estado());
        holder.foto.setImageBitmap(listaPartidos.get(position).getImagen());
        //holder.iconoAsociado.setImageBitmap(listaPartidos.get(position).getIconoAsociado());
        //holder.textSuscriptos.setText(listaPartidos.get(position).getCantSuscriptos());
        if (listaPartidos.get(position).getAsociados() == 0){
            holder.iconoAsociado.setVisibility(View.GONE);
        }
        else{
            holder.iconoAsociado.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return listaPartidos.size();
    }
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (listener != null) {
            listener.onClick(view);
        }
    }

    public class PartidosViewHolder extends RecyclerView.ViewHolder{

        TextView textFecha, textHora, textSuscriptos, textCancha, textComentario;
        ImageView foto, iconoAsociado;

        public PartidosViewHolder(View itemView) {
            super(itemView);
            textFecha = (TextView) itemView.findViewById(R.id.id_fecha);
            textHora = (TextView) itemView.findViewById(R.id.id_hora);
            textCancha = (TextView) itemView.findViewById(R.id.cancha);
            //textComentario = (TextView) itemView.findViewById(R.id.detalle_comentario);
            //textCategoria = (TextView) itemView.findViewById(R.id.id_categoria);
            //textEstado = (TextView) itemView.findViewById(R.id.id_estado);
            foto = (ImageView) itemView.findViewById(R.id.id_imagen);
            iconoAsociado = (ImageView) itemView.findViewById(R.id.imagen_asociado);
            iconoAsociado.setVisibility(GONE);
            //textSuscriptos = (TextView) itemView.findViewById(R.id.cant_suscriptos);
        }
    }
}
