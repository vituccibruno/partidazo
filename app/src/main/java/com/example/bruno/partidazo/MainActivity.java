package com.example.bruno.partidazo;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.bruno.partidazo.entidades.Jugador;
import com.example.bruno.partidazo.entidades.Partido;
import com.example.bruno.partidazo.entidades.Respuesta;
import com.example.bruno.partidazo.fragments.DetalleJugadorFragment;
import com.example.bruno.partidazo.fragments.DetallePartidoFragment;
import com.example.bruno.partidazo.fragments.DetalleRespuestaFragment;
import com.example.bruno.partidazo.fragments.ListaAddAsociadoFragment;
import com.example.bruno.partidazo.fragments.ListaDesasociarFragment;
import com.example.bruno.partidazo.fragments.ListaEstadosFragment;
import com.example.bruno.partidazo.fragments.ListaJugadoresFragment;
import com.example.bruno.partidazo.fragments.ListaPartidosFragment;
import com.example.bruno.partidazo.fragments.ListaRespuestasFragment;
import com.example.bruno.partidazo.fragments.PartidosFragment;
import com.example.bruno.partidazo.fragments.ProfileFragment;
import com.example.bruno.partidazo.fragments.RespuestaPartidoFragment;
import com.example.bruno.partidazo.fragments.SubirFragment;
import com.example.bruno.partidazo.fragments.dummy.DummyContent;
import com.example.bruno.partidazo.interfaces.ComunicacionFragments;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, PartidosFragment.OnListFragmentInteractionListener, ListaEstadosFragment.OnFragmentInteractionListener,
        DetallePartidoFragment.OnFragmentInteractionListener, ListaAddAsociadoFragment.OnFragmentInteractionListener, ListaDesasociarFragment.OnFragmentInteractionListener,
        RespuestaPartidoFragment.OnFragmentInteractionListener, ListaRespuestasFragment.OnFragmentInteractionListener, SubirFragment.OnFragmentInteractionListener,
        ListaJugadoresFragment.OnFragmentInteractionListener, DetalleRespuestaFragment.OnFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener,
        DetalleJugadorFragment.OnFragmentInteractionListener, ComunicacionFragments {

    private Locale locale;
    private Configuration config = new Configuration();
    ListaPartidosFragment listaPartidosFragment;
    ListaJugadoresFragment listaJugadoresFragment;
    DetalleJugadorFragment detalleJugadorFragment;
    DetallePartidoFragment detallePartidoFragment;
    DetalleRespuestaFragment detalleRespuestaFragment;
    ListaEstadosFragment listaEstadosFragment;
    ProfileFragment profileFragment;
    SubirFragment subirFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
        setContentView(R.layout.activity_main2);

        if (findViewById(R.id.contenedorFragment) != null){
            if (savedInstanceState != null){
                //getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaPartidosFragment).detach(listaPartidosFragment).attach(listaPartidosFragment).commit();
                return;
            }
            listaEstadosFragment = new ListaEstadosFragment();
            //getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaEstadosFragment).detach(listaEstadosFragment).attach(listaEstadosFragment).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaEstadosFragment).commit();
        }*/

        setContentView(R.layout.activity_main);
        /*
        if (findViewById(R.id.contenedorFragment) != null){
            if (savedInstanceState != null){
                //getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaPartidosFragment).detach(listaPartidosFragment).attach(listaPartidosFragment).commit();
                return;
            }
            listaEstadosFragment = new ListaEstadosFragment();
            //getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaEstadosFragment).detach(listaEstadosFragment).attach(listaEstadosFragment).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaEstadosFragment).commit();
        }*/
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {}

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_respuesta_reclamo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_profile) {
            ProfileFragment profileFragment;
            profileFragment = new ProfileFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, profileFragment).commit();
        }

        if (id == R.id.action_languaje) {
            mostrarDialog();
        }
        if (id == R.id.action_logout) { //cierra sesion
            SharedPreferences sharedPreferences = getSharedPreferences("sesion",MODE_PRIVATE); //toma la sesion actual del usuario
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.remove("usuario"); //cierra la sesion del usuario
            edit.commit();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.str_logout), Toast.LENGTH_LONG).show();
            this.finish(); //cierra el activity actual para q no se vuelva con back
        }
        switch (item.getItemId()) {
            case android.R.id.home: //hago un case por si en un futuro agrego mas opciones
                Log.i("ActionBar", "Atrás!");
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_armar_partido) {
            // Handle the camera action
            subirFragment = new SubirFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, subirFragment).commit();

        } else if (id == R.id.nav_ver_partidos) {
            listaEstadosFragment = new ListaEstadosFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaEstadosFragment).commit();
        }
        else if (id == R.id.nav_ver_jugadores) {
            listaJugadoresFragment = new ListaJugadoresFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, listaJugadoresFragment).commit();
        }
        //else if (id == R.id.nav_slideshow) { }
        else if (id == R.id.nav_perfil) {
            profileFragment = new ProfileFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, profileFragment).commit();

        }
        else if (id == R.id.nav_languaje) { //nav_share
            mostrarDialog();
        }
        else if (id == R.id.nav_logout) { //nav_send
            SharedPreferences sharedPreferences = getSharedPreferences("sesion",MODE_PRIVATE); //toma la sesion actual del usuario
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.remove("usuario"); //cierra la sesion del usuario
            edit.commit();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.str_logout), Toast.LENGTH_LONG).show();
            this.finish(); //cierra el activity actual para q no se vuelva con back
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void enviarPartido(Partido partido) {
        detallePartidoFragment = new DetallePartidoFragment();
        Bundle bundleEnvio = new Bundle();
        bundleEnvio.putSerializable("objeto", partido);
        detallePartidoFragment.setArguments(bundleEnvio);

        //cargar el fragment en el activity
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, detallePartidoFragment).commit(); //addToBackStack(null).
        //finish();
    }

    @Override
    public void enviarJugador(Jugador jugador) {
        detalleJugadorFragment = new DetalleJugadorFragment();
        Bundle bundleEnvio = new Bundle();
        bundleEnvio.putSerializable("objeto3", jugador);
        detalleJugadorFragment.setArguments(bundleEnvio);
        //cargar el fragment en el activity
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, detalleJugadorFragment).commit(); //addToBackStack(null).
    }

    @Override
    public void enviarRespuesta(Respuesta respuesta) {
        detalleRespuestaFragment = new DetalleRespuestaFragment();
        Bundle bundleEnvio = new Bundle();
        bundleEnvio.putSerializable("objeto2", respuesta);
        detalleRespuestaFragment.setArguments(bundleEnvio);

        //cargar el fragment en el activity
        getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, detalleRespuestaFragment).commit(); //addToBackStack(null).
    }

    @Override
    public void reejecutarGetHttpResponseDatosUser(){

    }


    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }

    private void mostrarDialog() { //
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle(getResources().getString(R.string.seleccion_idioma));
        //obtiene los idiomas del array de string.xml
        String[] types = getResources().getStringArray(R.array.languages);
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch (which) {
                    case 0:
                        locale = new Locale("en");
                        config.locale = locale;
                        break;
                    case 1:
                        locale = new Locale("es");
                        config.locale = locale;
                        break;
                    case 2:
                        locale = new Locale("it");
                        config.locale = locale;
                        break;
                    case 3:
                        locale = new Locale("ja");
                        config.locale = locale;
                        break;
                }
                getResources().updateConfiguration(config, null);
                Intent idiomasAlert = new Intent(MainActivity.this, MainActivity.class);
                startActivity(idiomasAlert);
                Toast.makeText(getApplicationContext(), getString(R.string.toast_idioma), Toast.LENGTH_LONG).show();
                finish();
            }
        });
        b.show();
    }
}
