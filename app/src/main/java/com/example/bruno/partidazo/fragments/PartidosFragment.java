package com.example.bruno.partidazo.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.bruno.partidazo.MainActivity;
import com.example.bruno.partidazo.MainActivity2;
import com.example.bruno.partidazo.MapActivity;
import com.example.bruno.partidazo.R;
import com.example.bruno.partidazo.entidades.Jugador;
import com.example.bruno.partidazo.entidades.Partido;
import com.example.bruno.partidazo.entidades.Respuesta;
import com.example.bruno.partidazo.fragments.dummy.DummyContent.DummyItem;
import com.example.bruno.partidazo.interfaces.ComunicacionFragments;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class PartidosFragment extends Fragment implements ComunicacionFragments {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    Button botonPorEstado, botonBuscar, botonPorCategoria;
    TextView textAbierto, textEncurso, textResuelto, textReabierto;
    ListaPartidosFragment listaPartidosFragment;
    DetallePartidoFragment detallePartidoFragment;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PartidosFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PartidosFragment newInstance(int columnCount) {
        PartidosFragment fragment = new PartidosFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_partidos, container, false);

        botonBuscar = rootView.findViewById(R.id.boton_buscar);
        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(adapterView.getContext(),(String) adapterView.getItemAtPosition(pos), Toast.LENGTH_SHORT).show();
                llamarFragmentPorPartido();
            }
        });
        /*
        final Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner_estado);
        String[] tipos1 = {"Abierto", "En curso", "Resuelto", "Re-abierto"};
        spinner.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, tipos1));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id)
            {
                String posicion = (String) adapterView.getItemAtPosition(pos);
                enviarEstado(posicion);
                //adapterView.getItemIdAtPosition(3);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {    }
        });*/
        return rootView;
    }

    private void enviarEstado(final String posicion) {
        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(adapterView.getContext(),(String) adapterView.getItemAtPosition(pos), Toast.LENGTH_SHORT).show();
                //SharedPreferences prefEstado = getContext().getSharedPreferences("estado", getContext().MODE_PRIVATE);
                //SharedPreferences.Editor editor = prefEstado.edit();
                //editor.putString("estadoNombre", posicion); //GUARDA EL ESTADO PARA USARLO EN EL MAIN 2
                //editor.commit();
                //llamarFragmentPorPartido();
            }
        });
    }

    //SPINNER NUEVO
    private void llamarFragmentPorPartido() {
        Intent intentVer = new Intent(getActivity(), MainActivity2.class);
        getActivity().startActivity(intentVer);
    }

    //SPINNER VIEJO
    private void llenarlistaEstado() {
        Intent intentVer = new Intent(getActivity(), MainActivity.class);
        getActivity().startActivity(intentVer);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void enviarPartido(Partido partido) {
        detallePartidoFragment = new DetallePartidoFragment();
        Bundle bundleEnvio = new Bundle();
        bundleEnvio.putSerializable("objeto", partido);
        detallePartidoFragment.setArguments(bundleEnvio);
        //getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragment, detallePartidoFragment).addToBackStack(null).commit();
    }

    @Override
    public void enviarRespuesta(Respuesta respuesta) {
        detallePartidoFragment = new DetallePartidoFragment();
        Bundle bundleEnvio = new Bundle();
        bundleEnvio.putSerializable("objeto2", respuesta);
        detallePartidoFragment.setArguments(bundleEnvio);
        //cargar el fragment en el activity
        //getSupportFragmentManager().beginTransaction().replace(R.id.container, detallePartidoFragment).addToBackStack(null).commit();
    }

    @Override
    public void enviarJugador(Jugador jugador) {}


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(DummyItem item);
    }

    public interface OnFragmentInteractionListener {
    }


    private void llamarIntentPorCategoria() {
    }

    /*
    private void llamarIntentVerMios() {
        //Intent intentVer = new Intent(getActivity(), EventosMiosActivity.class);
        Intent intentVer = new Intent(getActivity(), ActivityUser.class);
        getActivity().startActivity(intentVer);
    }*/

    private void llamarIntentMapa() { //pasa a un activity o fragment map
        Intent intentMap = new Intent(getActivity(), MapActivity.class);
        getActivity().startActivity(intentMap);

        /* si no funciona lo anterior...
        private final Context context;
        context = itemView.getContext();
        Intent detail = new Intent(context.getApplicationContext(), ImageDetail.class);
        detail.putExtra("id", imagen.getId());
        context.startActivity(detail);*/
    }

    @Override
    public void reejecutarGetHttpResponseDatosUser(){

    }
}
