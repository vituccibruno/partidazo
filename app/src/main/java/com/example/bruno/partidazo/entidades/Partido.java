package com.example.bruno.partidazo.entidades;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Bruno on 08/04/2018.
 */

public class Partido implements Serializable,Parcelable{
    private String id;
    private String id_usuario;
    private String fecha;
    private String hora;
    private Bitmap imagen;
    private Bitmap imagenDesc;
    private Image iconoAsociado;
    private String latitudDesc;
    private String longitudDesc;
    private String comentarioDesc;
    private String cancha;
    //private String cantSuscriptos;
    private int asociados;
    private int esAsociado;

    private int imagenId;
    private int imagenDescripcion;

    public Partido(){

    }

    //public Partido(String id, String usuarioDesc, String fecha, String latitudDesc, String longitudDesc, String motivo, String comentarioDesc, String estado, Bitmap imagen, Bitmap imagenDesc){ //int imagenDescripcion
    public Partido(String id, String id_usuario, String fecha, String hora, String cancha, Bitmap imagen, Bitmap imagenDesc, String latitudDesc, String longitudDesc,
                   String comentarioDesc){ // String cantSuscriptos, , int asociados, int esAsociado
        this.id = id;
        this.id_usuario = id_usuario;
        this.fecha = fecha;
        this.hora = hora;
        this.imagen = imagen;
        this.imagenDesc = imagenDesc;
        this.iconoAsociado = iconoAsociado;
        this.latitudDesc = latitudDesc;
        this.longitudDesc = longitudDesc;
        this.comentarioDesc = comentarioDesc;
        this.cancha = cancha;
        //this.cantSuscriptos = cantSuscriptos;
        this.asociados = asociados;
        this.esAsociado = esAsociado;
    }

    protected Partido(Parcel in) {
        id = in.readString();
        id_usuario = in.readString();
        fecha = in.readString();
        hora = in.readString();
        imagen = in.readParcelable(Bitmap.class.getClassLoader());
        imagenDesc = in.readParcelable(Bitmap.class.getClassLoader());
        //iconoAsociado = in.readParcelable(Image.class.getClassLoader());
        latitudDesc = in.readString();
        longitudDesc = in.readString();
        comentarioDesc = in.readString();
        cancha = in.readString();
        imagenId = in.readInt();
        imagenDescripcion = in.readInt();
        //cantSuscriptos = in.readString();
        asociados = in.readInt();
        esAsociado = in.readInt();
    }

    public static final Creator<Partido> CREATOR = new Creator<Partido>() {
        @Override
        public Partido createFromParcel(Parcel in) {
            return new Partido(in);
        }

        @Override
        public Partido[] newArray(int size) {
            return new Partido[size];
        }
    };

    public String getId() {return id;}

    public String getId_usuario(){return id_usuario;}
    public void setId_usuario(String id_usuario){this.id_usuario = id_usuario;}

    public String getFecha(){return fecha;}
    public void setFecha(String fecha){this.fecha = fecha;}

    public String getHora(){return hora;}
    public void setHora(String hora){this.hora = hora;}

    public String getLatitudDesc(){return latitudDesc;}
    public void setLatitudDesc(String latitudDesc){this.latitudDesc = latitudDesc;}

    public String getLongitudDesc(){return longitudDesc;}
    public void setLongitudDesc(String longitudDesc){this.longitudDesc = longitudDesc;}

    public String getComentarioDesc(){return comentarioDesc;}
    public void setComentarioDesc(String comentarioDesc){this.comentarioDesc = comentarioDesc;}

    public String getCancha(){return cancha;}
    public void setCancha(String cancha){this.cancha = cancha;}

    //public String getCantSuscriptos(){return cantSuscriptos;}
    //public void setCantSuscriptos(String cantSuscriptos){this.cantSuscriptos = cantSuscriptos;}

    //public int getImagenId(){return imagenId;}
    //public void setImagenId(int imagenId){this.imagenId = imagenId;}

    public Bitmap getImagen(){return imagen;}
    public void setImagen(Bitmap imagen){this.imagen = imagen;}

    public Bitmap getImagenDesc(){return imagenDesc;}
    public void setImagenDesc(Bitmap imagenDesc){this.imagenDesc = imagenDesc;}

    //public Image getIconoAsociado(){return iconoAsociado;}
    //public void setIconoAsociado(Image iconoAsociado){this.iconoAsociado = iconoAsociado;}

    public int getAsociados(){return asociados;}
    public void setAsociados(int asociados){this.asociados = asociados;}

    public int getEsAsociado(){return esAsociado;}
    public void setEsAsociado(int esAsociado){this.esAsociado = esAsociado;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(id_usuario);
        dest.writeString(fecha);
        dest.writeString(hora);
        dest.writeParcelable(imagen, flags);
        dest.writeParcelable(imagenDesc, flags);
        dest.writeString(latitudDesc);
        dest.writeString(longitudDesc);
        dest.writeString(comentarioDesc);
        dest.writeString(cancha);
        dest.writeInt(imagenId);
        dest.writeInt(imagenDescripcion);
        //dest.writeInt(iconoAsociado);
        //dest.writeString(cantSuscriptos);
        dest.writeInt(asociados);
        dest.writeInt(esAsociado);
    }

    //public int getImagenDescripcion(){return imagenDescripcion;}
    //public void setImagenDescripcion(int imagenDescripcion){this.imagenDescripcion = imagenDescripcion;}


}
